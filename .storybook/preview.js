import "../public/css/client.css"
import {Buffer} from "buffer"

globalThis.Buffer = Buffer;

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  viewport: {
    defaultViewport: 'tablet'
  },
}