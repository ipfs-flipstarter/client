// .storybook/main.js
const webpack = require('webpack');

// your app's webpack.config.js
const custom = require('../webpack.config.cjs');
const NodePolyfillWebpackPlugin = require('node-polyfill-webpack-plugin');

module.exports = {
  "stories": ["../stories/**/*.stories.mdx", "../stories/**/*.stories.@(js|jsx|ts|tsx)"],
  "addons": [
    "@storybook/addon-links", 
    "@storybook/addon-essentials", 
    "@storybook/addon-interactions", 
    {
      name: "@storybook/addon-postcss",
      options: {
        postcssLoaderOptions: {
          implementation: require('postcss'),
        },
      }
    }
  ],
  "framework": "@storybook/preact",
  webpackFinal: async (config) => {
    return { 
      ...config,
      resolve: {
        alias: {
          "react": "preact/compat",
          "react-dom/test-utils": "preact/test-utils",
          "react-dom": "preact/compat",     // Must be below test-utils
          "react/jsx-runtime": "preact/jsx-runtime"
        },
        fallback: {
          net: false,
          tls: false,
          http: false,
          https: false,
          crypto: require.resolve("crypto-browserify"),
          url: require.resolve("url/"),
          stream: require.resolve("stream-browserify"),
          buffer: require.resolve("buffer/"),
        }
      },
      module: { 
        ...config.module, 
        rules: [
          ...config.module.rules,
          {
            test: /.storybook\/preview.js/,
            resolve: { fullySpecified: false },
          },
          {
            test: /\.m?js$/,
            exclude: [
              /node_modules/, 
            ],
            use: {
              loader: "babel-loader",
              options: {
                presets: [
                  [
                    "@babel/preset-env",
                    {
                      targets: {
                        esmodules: true,
                      },
                    },
                  ],
                ],
                plugins: [
                  ["babel-plugin-transform-jsx-to-htm", {
                    "import": {
                      // the module to import:
                      "module": "htm/preact",
                      // a named export to use from that module:
                      "export": "html"
                    }
                  }]
                ]
              },
            },
          },
        ]
      }, 
      plugins: [
        ...config.plugins
      ]
    };
  },
  core: {
    builder: "webpack5"
  }
};