import { getWallet } from "@ipfs-flipstarter/utils/wallet/index.js"
import { useState, useEffect } from "preact/hooks";

export default function useWallet(seedPhrase) {
  const [wallet, setWallet] = useState(null);

  useEffect(() => {
    setWallet(getWallet(seedPhrase));
  }, [seedPhrase]);

  return wallet || {};
}