import { useQuery } from "react-query";
import { contributionQueryKeys } from "@ipfs-flipstarter/utils/queries/contributions.js";
import { useFundraiser } from "../withFundraiser.js";

export const useContributionsQuery = () => {
  const { recipients } = useFundraiser();
  return useQuery(contributionQueryKeys.list(recipients), {
    staleTime: 30 * 1000
  });
}