import { html } from "htm/preact"

export function Contribution() {
  return html`
    <li class="emptyContributionMessage">
      <i data-string="contributorEmpty1"></i>
      <br/>
      <i data-string="contributorEmpty2"></i>
    </li>
  `
}