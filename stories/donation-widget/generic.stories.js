import DonationWidget from "../../src/components/pages/landing/donations/donation-widget.js"

const Template = (args) => <div><DonationWidget {...args} /></div>;

export const NoContributions = Template.bind({});

// More on interaction testing: https://storybook.js.org/docs/preact/writing-tests/interaction-testing
export const SingleContribution = Template.bind({});

SingleContribution.args = {
  requestedAmount: 2000,
  amountRaised: 1000,
  contributionCount: 1,
}

SingleContribution.play = async ({ canvasElement }) => {
  // const canvas = within(canvasElement);
  // const loginButton = await canvas.getByRole('button', { name: /Log in/i });
  // await userEvent.click(loginButton);
};