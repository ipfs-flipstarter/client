export default {
  title: 'App/Sidebar/Desktop',
  decorators: [
    storyFn => (
      <div className="ds-storybook-grid">
        {storyFn()}
      </div>
    ),
  ],
  parameters: {
    viewport: {
      defaultViewport: 'tablet'
    }
  },
};

export * from "./generic.stories.js";