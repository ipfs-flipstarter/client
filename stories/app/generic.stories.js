import App from "../../src/components/app.js"
import { ContributionContext } from "../../src/components/withContributions.js"
import { WalletContext } from "../../src/components/withWallet.js";

import campaignJson from "../../public/campaign.json"

import { getWallet } from "@ipfs-flipstarter/utils/wallet/index.js";

const seedPhrase = 'bicycle demise goddess review true initial field agree oblige combine fame maximum';
const wallet = getWallet(seedPhrase);

const Template = (args) => <>
  <div>
    <WalletContext.Provider value={{ ...wallet, seedPhrase, utxos: [] }}>
      <ContributionContext.Provider value={{ contributions: args.contributions || []}}>
        <App {...args} />
      </ContributionContext.Provider>
    </WalletContext.Provider>
  </div>
</>;

export const WithoutImage = Template.bind({});

WithoutImage.args = {
  campaign: campaignJson
}

export const WithSmallImage = Template.bind({});

const campaginWithSmallImage = {
  ...campaignJson,
  image: "https://navbar.cloud.bitcoin.com/images/logo_black.png",
  recipients: [
    { 
      ...campaignJson.recipients[0], 
      image: "https://navbar.cloud.bitcoin.com/images/logo_black.png"
    },
    ...(campaignJson.recipients.slice(1))
  ],
}

WithSmallImage.args = {
  campaign: campaginWithSmallImage
}

export const WithLargeImage = Template.bind({});

const campaginWithLargeImage = {
  ...campaignJson,
  image: "https://flipstarter.paytaca.com/images/paytaca-app-header.png",
  recipients: [
    { 
      ...campaignJson.recipients[0], 
      image: "https://flipstarter.paytaca.com/images/paytaca-app-header.png"
    },
    ...(campaignJson.recipients.slice(1))
  ],
}

WithLargeImage.args = {
  campaign: campaginWithLargeImage
}

export const WithContributions = Template.bind({});
WithContributions.args = {
  campaign: campaginWithLargeImage,
  contributions: [{ 
    data: { 
      alias: "Tester 1", 
      comment: "This is a test comment" 
    }, 
    satoshis: 1000000, 
    cid: "" 
  }, { 
    data: { 
      alias: "Tester 2", 
      comment: "This is a second comment" 
    }, 
    satoshis: 1000, 
    cid: "" 
  }]
}