export default {
  title: 'App/Pages/Donate/Mobile',
  decorators: [
    storyFn => (
      <div className="ds-storybook-grid">
        {storyFn()}
      </div>
    ),
  ],
  parameters: {
    viewport: {
      defaultViewport: 'mobile1'
    }
  },
};

export * from "./generic.stories.js";