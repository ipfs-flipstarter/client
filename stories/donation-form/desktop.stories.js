export default {
  title: 'App/Pages/Donate/Desktop',
  decorators: [
    storyFn => (
      <div className="ds-storybook-grid">
        {storyFn()}
      </div>
    ),
  ],
  parameters: {
    viewport: {
      defaultViewport: 'tablet'
    }
  },
};

export * from "./generic.stories.js";