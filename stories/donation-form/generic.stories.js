import DonationPage from "../../src/components/pages/donation/index.js";
import campaignJson from "../../public/campaign.json";
import {WalletContext} from "../../src/components/withWallet.js";
import { ContributionContext } from "../../src/components/withContributions.js";

import { getWallet } from "@ipfs-flipstarter/utils/wallet/index.js";

const seedPhrase = 'bicycle demise goddess review true initial field agree oblige combine fame maximum';
const wallet = getWallet(seedPhrase);

const Template = (args) => <div>
  <WalletContext.Provider value={{ ...wallet, seedPhrase, utxos: [] }}>
    <ContributionContext.Provider value={{contributions: []}}>
      <DonationPage {...args} />
    </ContributionContext.Provider>
  </WalletContext.Provider>
</div>;

export const Default = Template.bind({});

Default.args = {
  campaign: {
    ...campaignJson,
    image: "https://flipstarter.paytaca.com/images/paytaca-app-header.png",
  },
  seedPhrase: 'bicycle demise goddess review true initial field agree oblige combine fame maximum',

}