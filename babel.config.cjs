module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          esmodules: false,
        },
      },
    ], 
    "@babel/preset-react"
  ],
  plugins: [
    ["@babel/transform-react-jsx", { "pragma": "preact.h", "pragmaFrag": "preact.Fragment" }],
    ["module-resolver", {
      "root": ["."],
      "alias": {
          "react": "preact/compat",
          "react-dom/test-utils": "preact/test-utils",
          "react-dom": "preact/compat",
          // Not necessary unless you consume a module using `createClass`
          "create-react-class": "preact/compat/lib/create-react-class",
          // Not necessary unless you consume a module requiring `react-dom-factories`
          "react-dom-factories": "preact/compat/lib/react-dom-factories"
      }
    }],
    [ 'babel-plugin-jsx-pragmatic', {
      module: 'preact',
      import: 'preact',
    }],
  ]
}