const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const NodePolyfillWebpackPlugin = require('node-polyfill-webpack-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');

require('dotenv').config({ path: `.env${process.env.NODE_ENV === "development" ? ".dev" : ""}` })

module.exports = {
  entry: path.join(__dirname, "./src/index.js"),
  output: {
    filename: "static/js/[name].js",
    path: path.join(__dirname, "./dist"),
    assetModuleFilename: 'static/media/[name][ext]'
  },
  externals: {
    ipfs: 'Ipfs'
  },
  resolve: {
    alias: {
      "react": "preact/compat",
      "react-dom/test-utils": "preact/test-utils",
      "react-dom": "preact/compat",     // Must be below test-utils
      "react/jsx-runtime": "preact/jsx-runtime"
    },
    fallback: {
      net: false,
      tls: false,
    }
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          keep_fnames: true,
          safari10: true,
          keep_classnames: true,
          mangle: {
            reserved: [
              'ecurve'
            ]
          }
        }
      }),
    ],
  },
  module: {
    rules: [
      { 
        test: /\.html$/, 
        use: {
          loader: "html-loader",  
          options: {
            esModule: false
          }
        },
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/css/[name].[ext]",
            },
          },
          { 
            loader: "extract-loader", 
            options: {
              publicPath: "../../" //important for relative path to "/static" folder (where file-loader places css compared to other files)
            }
          },
          {
            loader: "css-loader",
            options: {
              esModule: false, //important for extract-loader
            }
          },
          {
            loader: "postcss-loader",
          }
        ],
      },
      {
        include: [
          require.resolve("./public/js/ipfs.min.js"),
        ],
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/js/[name].[ext]",
              publicPath: ""
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif|woff2|woff|mp3|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "static/media/[name].[ext]",
              publicPath: ""
            },
          },
        ],
      },
      {
        test: /\.m?js$/,
        exclude: [
          /node_modules/, 
          require.resolve("./public/js/ipfs.min.js"),
        ],
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  targets: {
                    esmodules: true,
                  },
                },
              ],
            ],
            plugins: [
              ["babel-plugin-transform-jsx-to-htm", {
                "import": {
                  // the module to import:
                  "module": "htm/preact",
                  // a named export to use from that module:
                  "export": "html"
                }
              }]
            ]
          },
        },
      },
    ],
  },
  devtool: "source-map",
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          keep_fnames: true,
          safari10: true,
        },
      }),
    ],
  },
  plugins: [
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/wordlists\/(?!english)/, 
      contextRegExp: /bip39\/src$/
    }),
    // new BundleAnalyzerPlugin({ analyzerPort: 8889 }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './public/templates/client.html'),
      filename: 'index.html',
      //index.html at root
      publicPath: '',
      inject: 'body',
      favicon: path.join(__dirname, "./public/img/logo.ico"),
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/locale$/,
      contextRegExp: /moment$/,
    }),
    new webpack.EnvironmentPlugin({
      DEFAULT_GATEWAY_URL: process.env.DEFAULT_GATEWAY_URL || "https://flipstarter.me",
      DEFAULT_API_URL: process.env.DEFAULT_API_URL || "https://flipstarter.me",
      PRELOAD_NODES: [
        { url: "https://node0.preload.ipfs.io" },
        { url: "https://node1.preload.ipfs.io" },
        { url: "https://node2.preload.ipfs.io" },
        { url: "https://node3.preload.ipfs.io" },
        ...([{ 
          url: process.env.DEFAULT_API_URL, 
          multiaddr: process.env.DEFAULT_API_MULTIADDR
        }] || []),
      ],
      DEV_TIPS_ADDRESS: process.env.DEV_TIPS_ADDRESS || "bitcoincash:qpclad9r4zah39du3n55xj3mwdrkeuh0nyx8dqfqut",
      ELECTRUM_SERVERS: [
        { address: "bch.imaginary.cash", scheme: "wss" },
        { address: "electroncash.dk", scheme: "wss" },
        { address: "electrum.imaginary.cash", scheme: "wss" },
      ],
    }),

    new NodePolyfillWebpackPlugin()
  ],
}
